package io.llord.backend;

import net.sf.log4jdbc.sql.jdbcapi.DataSourceSpy;

import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

import io.llord.backend.impl.PropertyImpl;

import java.sql.SQLException;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
public class BackendConfig {

	@Autowired
	DataSourceProperties dataSourceProperties;

	@Bean
	Server server() throws SQLException {
		return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "8043");
	}

	@Bean
	@ConfigurationProperties(prefix = DataSourceProperties.PREFIX)
	DataSource realDataSource() {
		DataSource dataSource = DataSourceBuilder.create(this.dataSourceProperties.getClassLoader())
				.url(this.dataSourceProperties.getUrl()).username(this.dataSourceProperties.getUsername())
				.password(this.dataSourceProperties.getPassword()).build();
		return dataSource;
	}

	@Bean
	PropertyImpl propertyImpl() {
		return new PropertyImpl();
	}
	
	@Bean
	@Primary
	DataSource dataSource() {
		return new DataSourceSpy(realDataSource());
	}

	@Bean
    public HibernateJpaSessionFactoryBean sessionFactory(EntityManagerFactory emf) {
         HibernateJpaSessionFactoryBean factory = new HibernateJpaSessionFactoryBean();
         factory.setEntityManagerFactory(emf);
         return factory;
    }
}