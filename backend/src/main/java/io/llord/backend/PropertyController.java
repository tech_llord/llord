package io.llord.backend;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.llord.backend.domain.Property;
import io.llord.backend.impl.PropertyImpl;

@RestController
public class PropertyController {
	
    @Autowired
    PropertyImpl propertyImpl;
    
    @RequestMapping("/")
    public @ResponseBody List<Property> hello() {
    	List<Property> properties =  propertyImpl.propertyList();
    	
    	return properties;
    }
}
