package io.llord.backend.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import io.llord.backend.dao.PropertyRepository;
import io.llord.backend.domain.Property;

public class PropertyImpl {

	@Autowired
	PropertyRepository propertyRepo;
	
	public List<Property> propertyList() {
		List<Property> properties = new ArrayList<Property>();
		properties.add(new Property("40", "Foxes Piece", "Buckinghamshire", "SL7 1HE"));
		properties.add(new Property("83", "Mayville Road", "London", "E11 4PH"));
		
		propertyRepo.save(properties);
		
		return propertyRepo.findAll();
	}
}
