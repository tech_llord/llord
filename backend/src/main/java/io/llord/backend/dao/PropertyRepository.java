package io.llord.backend.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import io.llord.backend.domain.Property;

public interface PropertyRepository extends CrudRepository<Property, Integer> {
	
	public List<Property> findAll();
}
