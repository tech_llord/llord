package io.llord.backend.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "PROPERTY")
@NoArgsConstructor
@ToString
public class Property {

	@Id
	@Getter
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Getter
	@Setter
	@Column(name="HOUSE")
	private String house;

	@Getter
	@Setter
	@Column(name="STREET")
	private String street;
	
	@Getter
	@Setter
	@Column(name="ADDRESS")
	private String address;
	
	@Getter
	@Setter
	@Column(name="POSTCODE")
	private String postcode;

	public Property(String house, String street, String address, String postcode) {
		super();
		this.house = house;
		this.street = street;
		this.address = address;
		this.postcode = postcode;
	}
}
