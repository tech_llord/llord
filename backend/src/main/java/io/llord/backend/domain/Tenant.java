package io.llord.backend.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TENANT")
public class Tenant {

	@Id
	@Getter
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Getter
	@Setter
	@Column(name = "FIRSTNAME")
	private String firstName;

	@Getter
	@Setter
	@Column(name = "LASTNAME")
	private String lastName;

	@Getter
	@Setter
	@Column(name = "PHONENUMBER")
	private int phoneNumber;

	@Getter
	@Setter
	@Column(name = "HOUSE")
	private String house;

	@Getter
	@Setter
	@Column(name = "ADDRESS")
	private String address;

	@Getter
	@Setter
	@Column(name = "STREET")
	private String street;

	@Getter
	@Setter
	@Column(name = "POSTCODE")
	private String postcode;

	@Getter
	@Setter
	@Column(name = "SALARY")
	private int salary;

	@Getter
	@Setter
	@Column(name = "CITIZENSHIP")
	private String citizenship;

	@Getter
	@Setter
	@Column(name = "DATEOFBIRTH")
	private LocalDate dateOfBirth;

	@Getter
	@Setter
	@JoinColumn(name = "FKEY_CONTRACT")
	private String contractId;}
