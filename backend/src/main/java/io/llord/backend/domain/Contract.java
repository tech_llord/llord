package io.llord.backend.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "CONTRACT")
public class Contract {

	@Id
	@Getter
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Getter
	@Setter
	@Column(name = "RENT")
	private double rent;

	@Getter
	@Setter
	@Column(name = "START")
	private LocalDate start;

	@Getter
	@Setter
	@Column(name = "END")
	private LocalDate end;
	
	@Getter
	@JoinColumn(name="FKEY_PROPERTY")
	private int propertyId;
}
